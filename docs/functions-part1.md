# Python 함수: 함수의 정의 및 호출

## <a name="intro"></a> 들어가며
이 포스팅에서는 특정 작업을 수행하는 재사용 가능한 코드 블록인 함수를 정의하고 호출하는 방법을 설명할 것이다. 함수는 모듈식이고 체계적이며 효율적인 코드를 작성할 수 있기 때문에 프로그래밍에서 가장 중요한 개념 중 하나이다.

이 글이 끝날 때 다음 작업을 수행할 수 있을 것이다.

- 함수가 무엇이고 왜 유용한지 설명할 수 있다.
- `def` 키워드와 함수 이름을 사용하여 함수를 정의할 수 있다.
- 함수 이름과 괄호를 사용하여 함수를 호출할 수 있다.
- 매개 변수와 인수를 함수에 전달하여 함수의 동작을 커스터마이즈할 수 있다.
- `return` 문을 사용하여 함수에서 값을 반환할 수 있다.
- 함수 내부와 외부 변수의 범위와 수명 이해한다.
- 복잡한 문제를 해결하기 위해 중첩 함수와 재귀 함수 생성할 수 있다.
- 람다 함수를 사용하여 익명 함수를 한 줄에 작성할 수 있다.

이 포스팅을 따라가기 위해서는 Python과 데이터 분석에 대한 기본적인 이해가 필요하다. 또한 실습을 위하여 원하는 Python 편집기나 IDE를 사용하거나 [Repl.it](https://replit.com/) 같은 온라인 Python 인터프리터를 사용할 수 있다.

Python 함수에 대해 공부할 준비가 되었나요? 시작해 보자!

## <a name="sec_02"></a> 함수란?
함수는 특정한 작업을 수행하는 코드의 블록이다. 함수를 메인 프로그램에서 여러 번 재사용할 수 있는 미니 프로그램이라고 생각할 수 있다. 예를 들어, 원의 면적을 계산하는 함수를 작성한 다음, 원의 면적을 구해야 할 때마다 그 함수를 사용할 수 있다.

함수는 다음과 같은 이유 때문네 유용하다.

- 함수에서 관련 문을 함께 그룹화할 수 있기 때문에 코드를 보다 모듈화하고 조직저그으로 만들 수 있다.
- 동일한 코드를 반복하는 대신 함수를 한 번 작성하고 여러 번 사용할 수 있기 때문에 코드 복제와 중복을 줄일 수 있다.
- 함수에 의미 있는 이름을 부여하고 함수가 수행하는 작업을 문서화할 수 있기 때문에 코드 가독성과 유지보수성을 향상시킨다.
- 크고 복잡한 프로그램을 다루는 대신 각 기능을 개별적으로 테스트하고 수정할 수 있기 때문에 코드 테스트와 디버깅을 용이하게 한다.

Python에는 두 가지 종류의 함수가 있다:

- **내장 함수(built-in function)**: Python 표준 라이브러리에 이미 정의되어 있는 함수들로, 모듈을 가져오지 않고도 사용할 수 있다. 예를 들어 `print()`, `len()`, `type()`, `sum()` 등이 Python의 내장 함수들이다.
- **사용자 정의 함수(user-defined function)**: `def` 키워드와 함수 이름을 사용하여 개발자가 정의하는 함수들이다. 매개 변수와 인수를 사용자 정의 함수에 전달하고 그로부터 값을 반환함으로써 사용자 정의 함수의 동작을 사용자가 정의한다.

[다음 절](#sec_03)에서는 Python에서 사용자 정의 함수를 정의하고 호출하는 방법을 다룰 것이다.

## <a name="sec_03"></a> Python에서 함를 정의 방법
Python에서 함수를 정의하기 위해서는 `def` 키워드를 사용하고 함수 이름과 괄호를 사용해야 한다. 괄호 안에는 함수가 수용할 수 있는 하나 이상의 파라미터를 선택적으로 지정할 수 있다. 괄호 뒤에는 콜론(`:`)을 추가한 다음, 다음 행을 들여쓰기 한다. 코드의 들여쓰기 블록은 함수 본문으로, 함수가 실행할 문이다.

다음은 Python의 함수 정의에 대한 일반적인 구문이다.

```python
def function_name(parameter1, parameter2, ...):
    # function body
    # statements to execute
```

사용자에게 인사 메시지를 출력하는 간단한 함수의 예를 살펴보자. 함수명은 `greet`이며, `name`이라는 하나의 파라미터를 갖는다. 함수 본체는 "Hello,"와 `name` 파라미터의 값를 출력하는 하나의 문으로 구성된다.

```python
# Define a function called greet
    def greet(name):
        # Print a greeting message
        print("Hello, " + name + "!")
```

함수를 정의하는 것은 함수를 실행하는 것이 아니다. 함수를 실행하기 위해서는 함수를 호출해야 하는데, [다음 절](#sec_04)에서 설명할 것이다.

## <a name="sec_04"></a> Python에서 함수 호출 방법
Python에서 함수를 호출하기 위해서는 함수 이름 뒤에 괄호를 사용해야 한다. 괄호 안에는 함수 정의시의 매개 변수와 일치하는 하나 이상의 인수를 선택적으로 전달할 수 있다. 인수는 함수가 실행될 때 사용하기를 원하는 값이다.

다음은 Python에서 함수 호출의 일반적인 구문이다.

```python
function_name(argument1, argument2, ...)
```

[앞 절](#sec_03)에서 정의한 `greet` 함수를 호출하는 예를 살펴보자. 함수를 호출하려면 함수 정의시의 이름, 매개 변수와 일치하는 인수로 문자열 값을 전달해야 한다. 그러면 함수는 인수 값을 사용하여 인사 메시지를 인쇄할 것이다.

```python
# Call the greet function with "World" as an argument
greet("World")
# Output: Hello, World!

# Call the greet function with "Alice" as an argument
greet("Alice")
# Output: Hello, Alice!
```

함수 정의시의 매개변수와 일치한다면 다른 인수로 함수를 원하는 만큼 호출할 수 있다. 다른 함수 안에 있는 함수도 호출할 수 있는데, [중첩함수와 재귀함수에 대한 절](#sec_08)에서 살펴볼 것이다.

## <a name="sec_05"></a> 매개 변수와 인수
[앞 절](#sec_04)에서 매개변수가 하나이고 인수가 하나인 함수를 정의하고 호출하는 방법을 배웠다. 그러나 매개변수와 인수가 다수이거나 매개변수와 인수가 전혀 없는 함수도 정의하고 호출할 수 있다. 이 절에서 매개변수와 인수의 개념과 이들이 함수의 실행에 어떤 영향을 미치는지에 대해 자세히 알아볼 것이다.

매개변수와 인수는 자주 혼용되는 두 용어이지만 의미가 조금씩 다르다. 매개변수는 함수 정의에서 지정하는 변수이고, 인수는 함수를 부를 때 전달하는 값이다. 예를 들어 앞에서 정의한 `greet` 함수에서 `name`은 매개변수이고, `World`와 `Alice`는 인수이다.

매개 변수와 인수는 정의되고 함수에 전달되는 방식에 따라 다음과 같은 네 가지 유형으로 분류할 수 있다.

- **위치 매개변수와 인수**: 가장 일반적인 유형의 매개변수와 인수인데, 여기서 함수 정의에 있는 매개변수의 순서는 함수 호출에 있는 인수의 순서와 일치한다. 예를 들어 `a`와 `b`의 두 매개변수로 함수를 정의하고 `1`과 `2`의 인수로 호출하면 `a`는 `1`의 값을, `b`는 `2`의 값을 취할 것이다.
- **키워드 매개변수와 인수**: 등호(`=`)를 사용하여 함수 정의와 함수 호출에서 명시적으로 명명된 매개변수와 인수이다. 예를 들어 `a`와 `b`의 두 매개변수로 함수를 정의하고 `a=1`과 `b=2`의 두 매개변수로 함수를 호출하면 `a`는 `1`의 값을, `b`는 `2`의 값을 취할 것이다. 키워드 매개변수와 인수의 장점은 매개변수 이름을 지정하기만 하면 함수 호출에서 인수의 순서를 변경할 수 있다는 것이다. 예를 들어 `b=2`와 `a=1`로 함수를 호출하여도 이전과 동일한 결과를 얻을 수도 있다.
- **기본 매개변수와 인수**: 등호(`=`)를 사용하여 함수 정의에서 기본값을 지정하는 매개변수이다. 예를 들어 매개변수 `a`로 함수를 정의하고 기본값 `0`을 지정하면 함수 호출시 인수에 다른 값을를 전달하지 않는 한 `a`는 `0` 값을 받sms다. 기본 매개변수와 인수의 장점은 함수 호출에서 일부 인수를 선택적으로 만들 수 있고 대신 기본값을 사용할 수 있다는 것이다.
- **임의의 매개변수와 인수**: 함수 정의에서 별표(*) 또는 이중 별표(**)를 사용하여 함수 호출에서 임의의 수의 인수를 허용할 수 있는 매개변수이다. 예를 들어 매개변수인 *args로 함수를 정의하면 args는 함수에 전달하는 모든 위치 인수를 포함하는 튜플이 된다. 마찬가지로 매개변수인 **kwargs로 함수를 정의하면 kwargs는 함수에 전달하는 모든 키워드 인수를 포함하는 사전이 된다. 임의의 매개변수와 인수의 장점은 함수를 보다 유연하고 다양한 상황에 적응할 수 있다는 것이다.

[다음 절](#sec_06)에서는 함수에서 각 유형의 매개 변수와 인수를 사용하는 방법의 예를 볼 수 있다.

## <a name="sec_06"></a> 반환 값
지금까지 여러분은 화면에 일부를 출력하지만 프로그램에 어떤 값도 반환하지 않는 함수들을 보았다. 그러나 때로는 프로그램에서 나중에 사용할 수 있는 값을 반환하는 함수를 작성하기를 원할 수도 있다. 예를 들어, 원의 면적을 계산하는 함수를 작성한 다음 반환된 값을 사용하여 다른 계산을 수행하기를 원할 수 있다.

함수에서 값을 반환하려면 반환문 다음에 반환하려는 값을 사용해야 한다. 값은 숫자, 문자열, 목록, 사전 등과 같은 모든 데이터 타입일 수 있다. 함수에서 여러 값을 쉼표로 구분하여 반환할 수도 있다.

다음은 Python에서 반환문의 일반적인 구문이다.

```python
return value1, value2, ...
```

매개변수로 반지름이 주어졌을 때 원의 넓이를 반환하는 함수의 예를 살펴보자. 함수명은 `area_of_circle`이고, `radius`이라는 매개변수가 하나 있다. 함수 바디는 `pi * radius ** 2`라는 공식을 사용하여 넓이를 계산하고 결과를 반환하는 하나의 문장으로 구성된다. `pi` 값을 사용하기 위해 우리는 프로그램의 시작 부분에 수학 모듈을 임포트한다.

```python
# Import the math module
import math

# Define a function that returns the area of a circle
def area_of_circle(radius):
    # Calculate the area
    area = math.pi * radius ** 2
    # Return the area
    return area
```

함수에서 반환된 값을 사용하려면 함수를 호출할 때 변수에 할당해야 한다. 예를 들어 반환된 값을 `result`라는 변수에 할당한 다음 출력하거나 다른 계산에 사용할 수 있다.

```python
# Call the function with 5 as an argument and assign the returned value to result
result = area_of_circle(5)
# Print the result
print(result)
# Output: 78.53981633974483

# Use the result for some other calculation
print(result * 2)
# Output: 157.07963267948966
```

함수에서 하나의 값만 반환할 수 있지만 여러 값을 튜플, 목록 또는 사전으로 포장하여 반환할 수 있다. 예를 들어 반환문에서 쉼표로 구분하여 원의 넓이와 둘레를 반환하는 함수를 작성할 수 있다. 함수는 두 값을 모두 포함하는 튜플을 반환할 것이고, 함수를 호출할 때 두 개의 변수로 풀 수 있다.

```python
# Import the math module
import math

# Define a function that returns the area and the circumference of a circle
def area_and_circumference_of_circle(radius):
    # Calculate the area
    area = math.pi * radius ** 2
    # Calculate the circumference
    circumference = 2 * math.pi * radius
    # Return both values
    return area, circumference

# Call the function with 5 as an argument and unpack the returned values into two variables
area, circumference = area_and_circumference_of_circle(5)
# Print the area and the circumference
print(area)
print(circumference)
# Output: 78.53981633974483
# Output: 31.41592653589793
```

[다음 절](#sec_07)에서는 함수 내부와 외부 변수의 범위와 수명에 대해 알아볼 것이다.

## <a name="sec_07"></a> 변수의 범위와 수명
이 절에서는 Python에서 변수들의 범위와 수명, 그리고 변수들이 함수들의 행동에 어떤 영향을 미치는지에 대해 알아본다. 범위는 변수에 접근하여 수정할 수 있는 프로그램의 영역을 의미하며 수명은 메모리에 변수가 존재하는 프로그램의 기간을 의미한다.

Python에서는 그 범위에 따라 두 가지 타입의 변수가 있다.

- **전역 변수(global variable)**: 이들은 함수의 외부에서 정의된 변수들이며, 프로그램의 어떤 함수에 의해서도 액세스되고 수정할 수 있다. 예를 들어, 프로그램의 맨 위에 `x`라는 변수를 정의하고 그 변수에 `10`의 값을 할당하면, `x`는 프로그램의 어떤 함수에 의해서도 사용될 수 있는 전역 변수이다.
- **지역 변수(local variable)**: 함수 내부에서 정의한 변수들로 해당 함수에 의해서만 액세스와 수정이 가능하다. 예를 들어 `foo`라는 함수 내부에 `y`라는 변수를 정의하고 `20`의 값을 부여하면 `y`는 `foo` 함수에서만 사용할 수 있는 지역 변수이다.

전역 변수는 전역적 범위와 프로그램 수명을 가지며, 이는 프로그램 내의 어떤 함수에 의해서도 액세스와 수정이 가능하다는 것을 의미하며, 프로그램이 종료될 때까지 메모리에 존재한다. 지역 변수는 국지적 범위와 함수 수명을 가지며, 이는 정의된 함수에 의해서만 액세스와 수정이 가능하다는 것을 의미하며, 함수가 종료될 때까지만 메모리에 존재한다.

Python에서 전역 변수와 지역 변수가 어떻게 작동하는지에 대한 예를 살펴보자. 다음 프로그램에서 우리는 `x`라는 전역 변수를 정의하고 `10`의 값을 할당한다. 그런 다음 전역 변수 `x`를 다른 방식으로 사용하는 두 함수 `foo`와 `bar`를 정의한다. `foo` 함수는 `x`의 값을 출력하고 `bar` 함수는 x의 값에 1을 더해 수정한다. 마지막으로 우리는 두 함수를 호출하고 각 함수 호출 후에 `x`의 값을 출력한다.

```python
# Define a global variable x
x = 10

# Define a function foo that prints the value of x
def foo():
    # Print the value of x
    print(x)
# Define a function bar that modifies the value of x
def bar():
    # Use the global keyword to access the global variable x
    global x
    # Add 1 to x
    x = x + 1
# Call the foo function and print the value of x
foo()
print(x)
# Output: 10
# Output: 10
# Call the bar function and print the value of x
bar()
print(x)
# Output: 11
```

보다시피 `foo` 함수는 전역 변수 `x`를 액세스하여 그 값을 인쇄할 수는 있지만 수정할 수는 없다. `bar` 함수는 함수 내부의 변수 `x`가 함수 외부의 전역변수 `x`와 동일하다는 것을 Python에게 알려주는 `global` 키워드를 이용하여 전역 변수 `x`를 수정할 수 있다. `bar` 함수가 호출된 후 `x`의 값은 `10`에서 `11`로 변한다.

이제 Python에서 지역 변수가 어떻게 작동하는지에 대한 예를 살펴보자. 다음 프로그램에서 `foo` 함수 내부에 `y`라는 지역 변수를 정의하고 `20`의 값을 부여한다. 그런 다음 함수 내부와 외부에 `y`의 값을 인쇄하려고 한다.

```python
# Define a function foo that defines a local variable y
def foo():
    # Define a local variable y
    y = 20
    # Print the value of y
    print(y)

# Call the foo function
foo()
# Output: 20
# Try to print the value of y outside the function
print(y)
# Output: NameError: name 'y' is not defined
```

`foo` 함수는 `foo` 함수가 액세스하여 로컬 변수 `y`를 출력할 수 있지만 함수 외부에서는 접근할 수 없다. 로컬 변수 `y`는 `foo` 함수가 실행되는 동안에만 메모리에 존재하고 함수가 종료되면 삭제된다. 따라서 함수 외부에서 `y`의 값을 출력하려고 하면 `NameError`가 발생하며, 이는 변수 이름이 정의되어 있지 않음을 의미한다.

[다음 절](#sec_08)에서는 Python에서 중첩 함수와 재귀 함수를 만드는 방법을 설명할 것이다.

## <a name="sec_08"></a> 중첩 함수와 재귀 함수
이 절에서는 Python에서 중첩 함수와 재귀 함수를 만드는 방법을 설명할 것이다. 중첩 함수는 다른 함수 내부에서 정의되는 함수이고, 재귀 함수는 자신의 정의 안에서 스스로를 부르는 함수이다.

중첩 함수는 주 함수에서만 사용하는 도우미 함수를 만들고 이 포스팅에서 다루지 않을 고급 주제인 클로저와 데코레이터를 구현하는 데 유용하다. 재귀 함수는 팩토리얼, 피보나치, 이진 탐색 같이 같은 유형의 더 작고 간단한 하위 문제로 나눌 수 있는 문제를 해결하는 데 유용하다.

Python에서 중첩 함수의 예를 살펴보자. 다음 프로그램에서 기저(base)와 지수(exponent)의 두 매개변수를 취하고 기저 값에 지수로 거듭제곱하는 `power`라 불리는 함수를 정의한다. `power` 함수 안에서 우리는 `a`와 `b`의 두 매개변수를 취하고 `a`와 `b`의 곱을 반환하는 `multiply`이라 불리는 또 다른 함수를 정의한다. `multiply` 함수는 `powwer` 함수가 결과를 계산할 때만 사용하는 중첩 함수이다.

```python
# Define a function called power
def power(base, exponent):
    # Define a nested function called multiply
    def multiply(a, b):
        # Return the product of a and b
        return a * b
    # Initialize the result variable to 1
    result = 1
    # Loop from 0 to exponent
    for i in range(exponent):
        # Update the result by multiplying it with base
        result = multiply(result, base)
    # Return the result
    return result

# Call the power function with 2 and 3 as arguments
print(power(2, 3))
# Output: 8
```

보다시피 `power` 함수는 `multiply` 함수를 부를 수 있지만, `power`함수 밖에서는 `multiply` 함수를 액세스하거나 부를 수 없다. `multiply` 함수는 `power` 함수가 작동하는 동안에만 메모리에 존재하고, 함수가 끝나면 삭제된다.

이제 Python에서 재귀 함수의 예를 살펴보자. 다음 프로그램에서 우리는 `factorial`이라 불리는 함수를 정의하고 하나의 매개변수 `n`을 취하고 `n`보다 작거나 같은 모든 양의 정수의 곱인 `n`의 factorial을 반환한다. `factorial` 함수는 `n`이 `0` 또는 `1`에 도달할 때까지 1 작은 인수 값으로 자신의 정의 내에서 자신을 부르는 재귀 함수이다.

```python
# Define a function called factorial
def factorial(n):
    # Check if n is 0 or 1
    if n == 0 or n == 1:
        # Return 1 as the base case
        return 1
    else:
        # Return n multiplied by the factorial of n-1 as the recursive case
        return n * factorial(n-1)

# Call the factorial function with 5 as an argument
print(factorial(5))
# Output: 120
```

보다시피, `factorial` 함수는 기저(base) 사례에 도달할 때까지 더 작은 인수 값으로 자신을 호출할 수 있고, 기저(base) 사례에는 `1`을 반환한다. 재귀 호출은 스택에 저장되고 최종 결과는 스택에서 값을 제거하며 곱하여 계산된다.

재귀 함수는 우아하고 간결하지만 메모리를 많이 사용하고 재귀 깊이가 너무 길면 `StackOverflowError`를 발생시킬 수 있기 때문에 비효율적이고 디버그하기 어려울 수도 있다. 따라서 재귀 함수는 문제에 적합할 때만 주의해서 사용해야 한다.

[다음 절](#sec_09)에서는 람다 함수를 사용하여 익명 함수를 한 줄로 만드는 방법을 배울 것이다.

## <a name="sec_09"></a> 람다 함수
이 절에서는 람다 함수를 사용하여 한 줄로 익명 함수를 만드는 방법을 다룰 것이다. 람다 함수는 익명 함수 또는 람다 표현식으로도 알려져 있으며 맵, 필터, 정렬 등 다른 함수에 인수로 전달할 수 있는 간단한 함수를 만드는 데 유용하다.

람다 함수를 만들려면 `lambda` 키워드 다음에 하나 이상의 매개 변수와 콜론(`:`)을 사용하고 값을 반환하는 표현식을 사용해야 한다. 표현식은 유효한 Python 식일 수 있지만 `if`, `for` 또는 `return` 같은 문은 포함할 수 없다. 람다 함수를 변수에 할당할 수도 있고 다른 함수에 대한 인수로 직접 사용할 수도 있다.

다음은 Python에서 람다 함수의 일반적인 구문이다.

```python
lambda parameter1, parameter2, ...: expression
```

`a`와 `b`의 두 매개변수를 취하고 `a`와 `b`의 합을 반환하는 람다 함수의 예를 살펴보자. 람다 함수는 `add`라는 변수에 할당된 다음 `2`와 `3`이라는 두 개의 인수로 호출된다.

```python
# Define a lambda function that returns the sum of a and b
add = lambda a, b: a + b
# Call the lambda function with 2 and 3 as arguments
print(add(2, 3))
# Output: 5
```

보다시피 람다 함수는 다음과 같은 정규 함수와 동등하지만, 더 간결하고 함수 이름이나 반환문이 필요하지 않다.

```python
# Define a regular function that returns the sum of a and b
def add(a, b):
    # Return the sum of a and b
    return a + b
# Call the regular function with 2 and 3 as arguments
print(add(2, 3))
# Output: 5
```

람다 함수의 주요 사용 사례 중 하나는 함수를 입력으로 기대하는 다른 함수에 인수로 전달하는 것이다. 예를 들어, `map` 함수를 사용하여 반복 가능한 각 요소, 예를 들어 리스트에 람다 함수를 적용하고 결과와 함께 새로운 반복 가능한 것을 반환할 수 있다. `map` 함수는 함수와 반복 가능한 두 개의 인수를 사용하고 리스트, 튜플 또는 집합으로 변환할 수 있는 `map` 객체를 반환한다.

리스트의 각 요소를 제곱하는 람다 함수와 함께 `map` 함수를 사용하는 예를 살펴보자. 람다 함수는 하나의 매개변수 `x`를 취하고 `x`의 값을 제곱하여 반환한다. `map` 함수는 람다 함수와 리스트을 인수로 사용하고 리스트로 변환된 `map` 객체를 반환한다.

```python
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Use the map function with a lambda function that squares each element of the list
squares = list(map(lambda x: x ** 2, numbers))
# Print the list of squares
print(squares)
# Output: [1, 4, 9, 16, 25]
```

보다시피 람다 함수는 다음과 같은 정규 함수와 같은 결과를 생섷하지만, 보다 간결하고 함수명이 필요하지 않다.

```python
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Define a regular function that squares each element of the list
def square(x):
    # Return the value of x raised to the power of 2
    return x ** 2
# Use the map function with the regular function
squares = list(map(square, numbers))
# Print the list of squares
print(squares)
# Output: [1, 4, 9, 16, 25]
```

[다음 섹션](#conclusion)에서는 포스팅을 마무리하고자 한다.

## <a name="conclusion"></a> 마치며
Python 함수에 대한 이 포스팅을 마쳤다. 특정 작업을 수행하는 재사용 가능한 코드 블록인 함수를 정의하고 호출하는 방법을 배웠다. 매개변수와 인수를 함수에 전달하고, 함수로부터 값을 반환하고, 변수의 범위와 수명을 이해하고, 중첩 함수와 재귀 함수를 만들고, 람다 함수를 사용하는 방법도 배웠다.

함수를 활용하면 Python 프로그래밍에서 발생하는 여러 문제를 해결할 수 있는 모듈화, 조직화와 효율적인 코드를 작성할 수 있다. 함수는 프로그래밍에서 가장 중요한 개념 중 하나로 많은 어플리케이션과 프레임워크에서 널리 사용되고 있다.
