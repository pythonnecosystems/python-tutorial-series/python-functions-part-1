# Python 함수: 함수의 정의 및 호출 <sup>[1](#footnote_1)</sup>

> <font size="3">재사용 가능한 코드 블록인 함수를 정의하고 호출하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./functions-part1.md#intro)
1. [함수란?](./functions-part1.md#sec_02)
1. [Python에서 함를 정의 방법](./functions-part1.md#sec_03)
1. [Python에서 함수 호출 방법](./functions-part1.md#sec_04)
1. [매개 변수와 인수](./functions-part1.md#sec_05)
1. [반환 값](./functions-part1.md#sec_06)
1. [변수의 범위와 수명](./functions-part1.md#sec_07)
1. [중첩 함수와 재귀 함수](./functions-part1.md#sec_08)
1. [람다 함수](./functions-part1.md#sec_09)
1. [마치며](./functions-part1.md#conclusion)

<a name="footnote_1">1</a>: [Python Tutorial 13 — Python Functions: Defining and Calling Functions](https://levelup.gitconnected.com/python-tutorial-13-python-functions-defining-and-calling-functions-a9bec7782761?sk=f35bb6e3485e0b8011609463522e0e6c)를 편역한 것입니다.
